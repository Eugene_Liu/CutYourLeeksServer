import com.eugene.server.common.common.database.tidb.DBCP
import com.eugene.server.common.model.UsageRecord
import org.apache.commons.dbutils.QueryRunner
import org.apache.commons.dbutils.handlers.BeanHandler

fun main(){
    val queryRunner = QueryRunner(DBCP.driver)
    val usageRecord: UsageRecord? = queryRunner.query("select * from system_usage where indicator_code=?",BeanHandler(UsageRecord::class.java),"A")
    println(usageRecord.toString())
}
package com.eugene.server.BallGameApp.processor

import com.eugene.server.BallGameApp.entity.BattleGround
import io.netty.channel.ChannelHandlerContext
import io.netty.channel.SimpleChannelInboundHandler
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame

class FrameSyncProcessor : SimpleChannelInboundHandler<TextWebSocketFrame>() {
    override fun channelRead0(ctx: ChannelHandlerContext?, msg: TextWebSocketFrame?) {
        println("receive message" + msg!!.text())

        BattleGround.players.add(ctx!!.channel())

        ctx.channel().writeAndFlush(TextWebSocketFrame());
    }

    override fun handlerAdded(ctx: ChannelHandlerContext?) {
        println("connection established")
    }

    override fun handlerRemoved(ctx: ChannelHandlerContext?) {
        println("disconnect")
    }

    override fun exceptionCaught(ctx: ChannelHandlerContext?, cause: Throwable?) {
        println("Excetion occur: " + cause!!.message)
        ctx!!.close()
    }
}
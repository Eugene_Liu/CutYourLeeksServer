package com.eugene.server.BallGameApp

import com.eugene.server.collectorApp.processor.UsageProcessor
import com.eugene.server.common.bootstrap.Bootstrap
import com.eugene.server.common.config.ServerConfig
import com.eugene.server.common.core.initializer.HttpJsonProtocolInitializer
import com.eugene.server.common.core.initializer.WebsocketProtocolInitializer
import com.eugene.server.common.handler.Router
import com.eugene.server.common.util.logger
import io.netty.bootstrap.ServerBootstrap
import io.netty.channel.ChannelOption
import io.netty.channel.nio.NioEventLoopGroup
import io.netty.channel.socket.nio.NioServerSocketChannel
import io.netty.handler.logging.LogLevel
import io.netty.handler.logging.LoggingHandler
import java.net.InetSocketAddress

fun main() {
    Bootstrap.init()
    startCollector()
}

private fun startCollector() {
    HttpCollector().run()
}

private class HttpCollector {
    fun run() {
        val startTimestamp = System.currentTimeMillis()
        val bossGroup = NioEventLoopGroup()
        val workerGroup = NioEventLoopGroup()
        try {
            val serverBootstrap = ServerBootstrap()
            serverBootstrap.group(bossGroup, workerGroup)
                .channel(NioServerSocketChannel::class.java)
                .handler(LoggingHandler(LogLevel.INFO))
                .childHandler(WebsocketProtocolInitializer())
                .option(ChannelOption.SO_BACKLOG,1024)

            val channelFuture = serverBootstrap.bind(InetSocketAddress(ServerConfig.INSTANCE.bindIp, ServerConfig.INSTANCE.bindPort)).sync()
            logger.info("${ServerConfig.INSTANCE.appName} start on port ${ServerConfig.INSTANCE.bindIp + ":" + ServerConfig.INSTANCE.bindPort}, spend ${System.currentTimeMillis() - startTimestamp}ms")
            channelFuture.channel().closeFuture().sync()
        } finally {
            bossGroup.shutdownGracefully()
            workerGroup.shutdownGracefully()
        }
    }
}
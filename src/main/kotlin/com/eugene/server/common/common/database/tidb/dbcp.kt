package com.eugene.server.common.common.database.tidb

import com.eugene.server.common.core.constant.appPath
import org.apache.commons.dbcp2.BasicDataSource
import org.apache.commons.dbcp2.BasicDataSourceFactory
import java.io.File
import java.util.*

class DBCP {
    companion object{
        var driver: BasicDataSource
        init {
            val configurationStream = File("$appPath/config/dbcp.properties").inputStream()
            val configurationProperties = Properties()
            configurationProperties.load(configurationStream)
            driver = BasicDataSourceFactory.createDataSource(configurationProperties)
        }
    }
}
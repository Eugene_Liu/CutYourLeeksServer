package com.eugene.server.common.common.database.tidb

import com.eugene.server.common.model.UsageRecord
import org.apache.commons.dbutils.QueryRunner

class TidbOps private constructor() {
    companion object {
        private const val insertUsageRecord =
            "insert into system_usage(indicator_code,report_node,usage_node,usage_identity,usage_amount,child_usage_node,child_usage_amount,timestamp)" +
                    "values (?,?,?,?,?,?,?,?)"
        private const val queryUsageRecord = ""

        fun insertUsageRecord(usageRecord: UsageRecord) {
            val queryRunner = QueryRunner(DBCP.driver)
            queryRunner.update(
                insertUsageRecord,
                usageRecord.indicator_code,
                usageRecord.report_node,
                usageRecord.usage_node,
                usageRecord.usage_identity,
                usageRecord.usage_amount,
                usageRecord.child_usage_node,
                usageRecord.child_usage_amount,
                usageRecord.timestamp
            )
        }

        fun insertUsageRecords(usageRecords: List<UsageRecord>) {
            usageRecords.forEach {
                insertUsageRecord(it)
            }
        }
    }
}
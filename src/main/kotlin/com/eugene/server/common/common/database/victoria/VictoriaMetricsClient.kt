package com.eugene.server.common.common.database.victoria

import com.eugene.server.common.util.logger
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody

// TODO: 池化VM Client

class VictoriaMetricsClient {
    companion object {
        // 使用如下的方式将数据写入到vm series db
        // curl -d 'measurement,tag1=value1,tag2=value2 field1=123,field2=1.23' -X POST 'http://localhost:8428/write'
        fun writeData(vmData: VMData) {
            val mediaType = "text/plain".toMediaType()
            val okHttpClient = OkHttpClient()
            val request = Request.Builder()
                    .url(VM_WRITE_API)
                    .post(RequestBody.create(mediaType,vmData.toString()))
                    .build()
            val response = okHttpClient.newCall(request).execute()
            logger.info(response.body?.string())
        }
    }
}
package com.eugene.server.common.common.database.victoria

import com.eugene.server.common.config.ServerConfig
private val VM_BASE_URL = "http://${ServerConfig.INSTANCE.victoriaMetricsConfig.ip}:${ServerConfig.INSTANCE.victoriaMetricsConfig.port}"
val VM_WRITE_API = "${VM_BASE_URL}/write"
package com.eugene.server.common.common.database.victoria

//                                                                           1635407803
// 'measurement,tag9=value1,tag12=value2 field3=1232329999,field4=1922999.99 1635251178888000000'
class VMData {
    var tags :HashMap<String,String> = HashMap()
    var fields: MutableList<String> = ArrayList()
    var timestamp:Int = 0

    override fun toString(): String {
        val stringBuilder = StringBuilder("measurement")
        tags.forEach { (tagName, tagValue) ->
            stringBuilder.append(",${tagName}=${tagValue}")
        }

        stringBuilder.append(" ")

        fields.forEachIndexed { index, field ->
            stringBuilder.append("field${index + 1}=${field}")
            if(index != fields.size - 1) stringBuilder.append(",")
        }

        stringBuilder.append(" ")

        // ns timestamp to vm
        stringBuilder.append("${timestamp}000000000" )

        return stringBuilder.toString()
    }
}
package com.eugene.server.common.common.database

import com.eugene.server.common.common.database.tidb.TidbOps
import com.eugene.server.common.common.database.victoria.VMData
import com.eugene.server.common.config.ServerConfig
import com.eugene.server.common.model.UsageRecord
import java.util.*

class DBOperator private constructor(){
    companion object{
        fun saveUsageRecords(usageRecordList:List<UsageRecord>){

            // 目前默认方案
            fun saveToTiDB(usageRecords: List<UsageRecord>) {
                TidbOps.insertUsageRecords(usageRecords)
            }

            fun saveToVM(usageRecords: List<UsageRecord>) {
                val vmDataList = LinkedList<VMData>()
                // 该方案暂时不用  用的时候需要将对应数据转换补全 并将数据通过http协议传送到对应的vm服务器上

            }

            when (ServerConfig.INSTANCE.useDb) {
                "tidb" -> {
                    saveToTiDB(usageRecordList)
                }
                "vm" -> {
                    saveToVM(usageRecordList)
                }
                else -> {
                    saveToTiDB(usageRecordList)
                }
            }
        }
    }
}
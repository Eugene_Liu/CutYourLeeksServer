package com.eugene.server.common.model

data class UsageRecord(
    var indicator_code: String = "",
    var report_node: String = "",
    var usage_node: String = "",
    var usage_identity: String = "",
    var usage_amount: Float = 0f,
    var child_usage_node: String = "",
    var child_usage_amount: Float = 0f,
    var timestamp: Int = 0
)

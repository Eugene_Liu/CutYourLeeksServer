package com.eugene.server.common.filter

interface Filter<Req> {
    fun filter(req: Req): Boolean
}
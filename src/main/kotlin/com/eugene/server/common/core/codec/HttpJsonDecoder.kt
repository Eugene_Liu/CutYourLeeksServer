package com.eugene.server.common.core.codec

import com.alibaba.fastjson.JSON
import com.alibaba.fastjson.JSONObject
import com.eugene.server.common.handler.HttpJsonRequest
import com.eugene.server.common.util.buildErrorResponse
import io.netty.buffer.ByteBuf
import io.netty.channel.ChannelFutureListener
import io.netty.channel.ChannelHandlerContext
import io.netty.handler.codec.MessageToMessageDecoder
import io.netty.handler.codec.http.FullHttpRequest
import io.netty.handler.codec.http.HttpResponseStatus
import io.netty.util.CharsetUtil.UTF_8

class HttpJsonDecoder : MessageToMessageDecoder<FullHttpRequest>() {

    override fun decode(channelHandlerContext: ChannelHandlerContext?,
                        fullHttpRequest: FullHttpRequest?,
                        resultList: MutableList<Any>?) {
        if (fullHttpRequest!!.decoderResult().isFailure) {
            channelHandlerContext!!.writeAndFlush(buildErrorResponse(HttpResponseStatus.BAD_REQUEST,"Failure: request format error")).addListener(ChannelFutureListener.CLOSE)
            return
        }

        val httpJsonRequest = HttpJsonRequest(fullHttpRequest,decode(fullHttpRequest.content()))
        resultList!!.add(httpJsonRequest)
    }

    private fun decode(jsonBody: ByteBuf): JSONObject {
        val content = jsonBody.toString(UTF_8)
        return JSON.parseObject(content)
    }

}
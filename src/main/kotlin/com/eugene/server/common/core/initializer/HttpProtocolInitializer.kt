package com.eugene.server.common.core.initializer

import com.eugene.server.common.core.codec.HttpJsonDecoder
import com.eugene.server.common.core.codec.HttpJsonEncoder
import com.eugene.server.common.handler.JsonRequestDispatcher
import io.netty.channel.ChannelInitializer
import io.netty.channel.socket.SocketChannel
import io.netty.handler.codec.http.HttpObjectAggregator
import io.netty.handler.codec.http.HttpRequestDecoder
import io.netty.handler.codec.http.HttpResponseEncoder

class HttpJsonProtocolInitializer: ChannelInitializer<SocketChannel>() {
    override fun initChannel(socketChannel: SocketChannel?) {
        socketChannel?.let {
            val pipeline = it.pipeline()
            pipeline.addLast("http-request-decoder",HttpRequestDecoder())
                    .addLast("http-object-aggregator",HttpObjectAggregator(65536))
                    .addLast("http-json-decoder",HttpJsonDecoder())
                    .addLast("http-response-encoder",HttpResponseEncoder())
                    .addLast("http-json-encoder",HttpJsonEncoder())
                    .addLast(JsonRequestDispatcher())
        }

    }
}
package com.eugene.server.common.core.codec

import com.alibaba.fastjson.JSON
import com.eugene.server.common.handler.HttpJsonResponse
import io.netty.buffer.ByteBuf
import io.netty.buffer.Unpooled
import io.netty.channel.ChannelHandlerContext
import io.netty.handler.codec.MessageToMessageEncoder
import io.netty.handler.codec.http.DefaultFullHttpResponse
import io.netty.handler.codec.http.HttpHeaderNames.CONTENT_LENGTH
import io.netty.handler.codec.http.HttpHeaderNames.CONTENT_TYPE
import io.netty.handler.codec.http.HttpResponseStatus
import io.netty.handler.codec.http.HttpVersion
import io.netty.util.CharsetUtil.UTF_8

class HttpJsonEncoder : MessageToMessageEncoder<HttpJsonResponse>() {
    override fun encode(channelHandlerContext: ChannelHandlerContext?, httpJsonResponse: HttpJsonResponse?, resultList: MutableList<Any>?) {
        val responseByteBuf = encode(httpJsonResponse!!.getJsonBody())
        val fullHttpResponse = DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK,responseByteBuf)
        fullHttpResponse.headers()
                .set(CONTENT_TYPE,"text/json")
                .set(CONTENT_LENGTH,responseByteBuf.readableBytes())
        resultList!!.add(fullHttpResponse)
    }

    fun encode(jsonBody:Any):ByteBuf {
        val jsonStr = JSON.toJSONString(jsonBody)
        return Unpooled.copiedBuffer(jsonStr,UTF_8)
    }
}
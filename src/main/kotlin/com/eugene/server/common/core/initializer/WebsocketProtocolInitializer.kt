package com.eugene.server.common.core.initializer

import com.eugene.server.BallGameApp.processor.FrameSyncProcessor
import io.netty.buffer.Unpooled
import io.netty.channel.ChannelInitializer
import io.netty.channel.ChannelPipeline
import io.netty.channel.socket.SocketChannel
import io.netty.handler.codec.DelimiterBasedFrameDecoder
import io.netty.handler.codec.LineBasedFrameDecoder
import io.netty.handler.codec.http.HttpObjectAggregator
import io.netty.handler.codec.http.HttpServerCodec
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler
import io.netty.handler.codec.string.StringDecoder
import io.netty.handler.stream.ChunkedWriteHandler
import java.nio.charset.StandardCharsets

class WebsocketProtocolInitializer :ChannelInitializer<SocketChannel>() {
    override fun initChannel(ch: SocketChannel) {
        val pipeline = ch.pipeline()
        pipeline.addLast(HttpServerCodec())
        pipeline.addLast(ChunkedWriteHandler())
        pipeline.addLast(HttpObjectAggregator(8192))
        pipeline.addLast(LineBasedFrameDecoder(1024))
        pipeline.addLast(StringDecoder())
//        pipeline.addLast(DelimiterBasedFrameDecoder(1024, Unpooled.copiedBuffer(("$"+"_").toByteArray(StandardCharsets.UTF_8))))
        pipeline.addLast(WebSocketServerProtocolHandler("/emit"))
        pipeline.addLast(FrameSyncProcessor())

    }
}
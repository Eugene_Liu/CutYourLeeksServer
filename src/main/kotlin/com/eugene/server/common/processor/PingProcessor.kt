package com.eugene.server.common.processor

import com.eugene.server.common.core.annotation.RequestMapping
import com.eugene.server.common.handler.HttpJsonRequest
import io.netty.buffer.Unpooled
import io.netty.channel.ChannelHandlerContext
import io.netty.handler.codec.http.DefaultFullHttpResponse
import io.netty.handler.codec.http.HttpHeaderNames
import io.netty.handler.codec.http.HttpResponseStatus
import io.netty.handler.codec.http.HttpVersion
import io.netty.util.CharsetUtil

class PingProcessor : Processor<HttpJsonRequest> {

    @RequestMapping(method = "GET", path = "/ping")
    override fun process(channelHandlerContext: ChannelHandlerContext, request: HttpJsonRequest) {
        val message = Unpooled.copiedBuffer("Pong\r\n", CharsetUtil.UTF_8)
        val fullHttpResponse = DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK, message)
        val httpHeaders = fullHttpResponse.headers()
        httpHeaders.set(HttpHeaderNames.CONTENT_LENGTH.toString(), message.readableBytes())

        channelHandlerContext.writeAndFlush(fullHttpResponse)
        return
    }
}
package com.eugene.server.common.processor

import com.eugene.server.common.filter.Filter
import com.eugene.server.common.util.logger
import io.netty.channel.ChannelHandlerContext


interface Processor<Req> : Filter<Req> {

    fun _process(channelHandlerContext: ChannelHandlerContext,request: Req){
        if(filter(request)) {
            process(channelHandlerContext,request)
        } else {
            logger.info("$request is filtered")
        }
    }

    fun process(channelHandlerContext: ChannelHandlerContext, request: Req)

    override fun filter(req: Req): Boolean {
        return true
    }
}
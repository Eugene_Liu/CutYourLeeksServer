package com.eugene.server.common.handler

import com.alibaba.fastjson.JSONObject
import io.netty.handler.codec.http.FullHttpRequest

class HttpJsonRequest(fullHttpRequest: FullHttpRequest, private var jsonBody: JSONObject) :
    BaseRequest(fullHttpRequest) {
    fun getJsonBody() = jsonBody
}
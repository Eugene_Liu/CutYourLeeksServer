package com.eugene.server.common.handler

import io.netty.handler.codec.http.FullHttpRequest

open class BaseRequest(private var fullHttpRequest: FullHttpRequest) {
    fun getFullHttpRequest() = fullHttpRequest
}
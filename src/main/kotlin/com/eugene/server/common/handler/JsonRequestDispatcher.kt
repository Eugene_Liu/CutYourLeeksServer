package com.eugene.server.common.handler

import com.eugene.server.common.processor.Processor
import com.eugene.server.common.util.buildErrorResponse
import com.eugene.server.common.util.logger
import io.netty.channel.ChannelHandlerContext
import io.netty.channel.SimpleChannelInboundHandler
import io.netty.handler.codec.http.HttpResponseStatus

class JsonRequestDispatcher : SimpleChannelInboundHandler<HttpJsonRequest>() {
    override fun channelRead0(channelHandlerContext: ChannelHandlerContext?, httpJsonRequest: HttpJsonRequest?) {
        if (channelHandlerContext != null && httpJsonRequest != null) {
            val fullHttpRequest = httpJsonRequest.getFullHttpRequest()
            val requestUri = fullHttpRequest.uri()
            val processor :Processor<in BaseRequest>? = Router.getRouter(requestUri)
            processor?.let {
                processor._process(channelHandlerContext, httpJsonRequest)
                return
            }
            channelHandlerContext.writeAndFlush(buildErrorResponse(HttpResponseStatus.NOT_FOUND, "Wrong uri"))
            logger.warn("Illegal uri request: $requestUri")
            return
        }
        throw RuntimeException("channelHandlerContext or fullHttpRequest is null")
    }

}
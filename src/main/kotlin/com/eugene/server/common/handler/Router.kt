package com.eugene.server.common.handler

import com.eugene.server.common.processor.PingProcessor
import com.eugene.server.common.processor.Processor

private val router = HashMap<String, Any>().apply {
    this["/ping"] = PingProcessor()
}

class Router {
    companion object{
        fun registerRouter(path: String, processor: Processor<out BaseRequest>) {
            router[path] = processor
        }

        fun getRouter(path:String): Processor<in BaseRequest>? {
            val processor = router[path]
            return if(processor == null) {
                null
            }  else {
                processor as Processor<in BaseRequest>
            }
        }
    }
}

package com.eugene.server.common.util

import io.netty.buffer.Unpooled
import io.netty.handler.codec.http.*
import io.netty.util.CharsetUtil.UTF_8

fun buildErrorResponse(status: HttpResponseStatus, message: String): FullHttpResponse {
    val byteBuf = Unpooled.copiedBuffer(message, UTF_8)
    val fullHttpResponse = DefaultFullHttpResponse(HttpVersion.HTTP_1_1, status, byteBuf)
    fullHttpResponse.headers().set(HttpHeaderNames.CONTENT_TYPE, "application/json")
    return fullHttpResponse
}

fun buildSuccessResponse(message: String): FullHttpResponse {
    val byteBuf = Unpooled.copiedBuffer(message, UTF_8)
    val fullHttpResponse = DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK,byteBuf)
    fullHttpResponse.headers().set(HttpHeaderNames.CONTENT_TYPE, "application/json")
    return fullHttpResponse
}
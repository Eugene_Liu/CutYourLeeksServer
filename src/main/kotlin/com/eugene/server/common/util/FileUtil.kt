package com.eugene.server.common.util

import com.eugene.server.common.core.constant.appPath
import java.io.File

fun findConfigFile(name: String): File? {
    val file = File("${appPath}/config/${name}")
    return if (file.exists()) file else null
}
package com.eugene.server.common.config

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
class ServerConfig {
    @JsonProperty("appName")
    lateinit var appName: String

    @JsonProperty("bindIp")
    lateinit var bindIp: String

    @JsonProperty("bindPort")
    var bindPort: Int = 0

    @JsonProperty("useDb")
    lateinit var useDb : String

    @JsonProperty("tidb")
    lateinit var tiDbConfig: TiDbConfig

    @JsonProperty("victoriaMetrics")
    lateinit var victoriaMetricsConfig :VictoriaMetricsConfig

    @JsonProperty("kafka")
    lateinit var kafkaConfig : KafkaConfig

    companion object {
        lateinit var INSTANCE : ServerConfig
    }
}

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
class TiDbConfig {
    @JsonProperty("ip")
    lateinit var ip:String

    @JsonProperty("port")
    lateinit var port:String
}

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
class VictoriaMetricsConfig {
    @JsonProperty("ip")
    lateinit var ip : String

    @JsonProperty("port")
    var port : Int = 0
}

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
class KafkaConfig {
    @JsonProperty("topicName")
    lateinit var topicName : String
}
package com.eugene.server.common.bootstrap

import com.eugene.server.common.config.ServerConfig
import com.eugene.server.common.util.findConfigFile
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.apache.logging.log4j.core.config.Configurator

class Bootstrap {
    companion object {
        fun init() {
            val serverConfigFile = findConfigFile("server_config.json")!!
            val jacksonObjectMapper = jacksonObjectMapper()
            ServerConfig.INSTANCE = jacksonObjectMapper.readValue(serverConfigFile)
            System.setProperty("hostname","${ServerConfig.INSTANCE.bindIp}:${ServerConfig.INSTANCE.bindPort}" )

            val log4jConfigFile = findConfigFile("log4j2.xml")!!
            Configurator.initialize("log4j2", log4jConfigFile.absolutePath)
        }
    }
}
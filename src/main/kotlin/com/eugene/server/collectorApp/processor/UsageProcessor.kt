package com.eugene.server.collectorApp.processor

import com.alibaba.fastjson.JSONObject
import com.eugene.server.common.common.database.DBOperator
import com.eugene.server.common.handler.HttpJsonRequest
import com.eugene.server.common.model.UsageRecord
import com.eugene.server.common.processor.Processor
import com.eugene.server.common.util.buildSuccessResponse
import com.eugene.server.common.util.logger
import io.netty.channel.ChannelHandlerContext
import java.util.*

// 上报数据形式
// 用量加权形式数据上报
//{
//	"indicator_code": "metis_storage_size",
//	"report_node": "metis",
//	"timestamp": 1635394293,
//	"usage_nodes": [{
//			"midas": {
//				"usage_identity": "cbs_midas_log",
//				"usage_amount": 1024
//			}
//		},
//		{
//			"feed": {
//				"usage_identity": "cbs_feed_log",
//				"usage_amount": 512
//			}
//		}
//	]
//}
//
//
// 情况二 ： 真实成本策略形式数据上报
// {
//	"indicator_code": "metis_storage_size",
//	"report_node": "metis",
//	"timestamp": 1635394293,
//	"usage_nodes": [{
//			"midas": {
//				"usage_identity": "cbs_midas_log",
//				"usage_detail": {
//					"collector": 512,
//					"tailer": 512
//				}
//			}
//		},
//		{
//			"feed": {
//				"usage_identity": "cbs_feed_log",
//				"usage_detail": {
//					"collector": 256,
//					"tailer": 768
//				}
//			}
//		}
//	]
//}
//
//
//
class UsageProcessor : Processor<HttpJsonRequest> {

    override fun process(channelHandlerContext: ChannelHandlerContext, request: HttpJsonRequest) {
        val startTimeMills = System.currentTimeMillis()
        val jsonBody = request.getJsonBody()
//        logger.info(jsonBody)

        val usageRecordList = LinkedList<UsageRecord>()
        val indicatorCode = jsonBody.getString("indicator_code")
        val timestamp = jsonBody.getIntValue("timestamp")
        val reportNode = jsonBody.getString("report_node")
        val usageNodes = jsonBody.getJSONArray("usage_nodes")
        usageNodes.forEach {
            val jsonObject = it as JSONObject
            jsonObject.entries.forEach {
                val usageNode = it.key
                val usageValue = it.value as JSONObject

                val usageIdentity = usageValue.getString("usage_identity")
                val usageAmount = usageValue.getFloat("usage_amount")

                if (usageAmount == null) {
                    // 真实成本数据上报
                    val usageDetail = usageValue.getJSONObject("usage_detail")

                    usageDetail.entries.forEach { usageDetailEntry ->
                        val usageRecord = UsageRecord(
                            indicator_code = indicatorCode,
                            report_node = reportNode,
                            usage_node = usageNode,
                            usage_identity = usageIdentity,
                            timestamp = timestamp,
                            child_usage_node = usageDetailEntry.key,
                            child_usage_amount = (usageDetailEntry.value as Int).toFloat()
                        )
                        usageRecordList.add(usageRecord)
                    }
                } else {
                    // 用量加权数据上报
                    val usageRecord = UsageRecord(
                        indicator_code = indicatorCode,
                        report_node = reportNode,
                        usage_node = usageNode,
                        usage_identity = usageIdentity,
                        timestamp = timestamp,
                        usage_amount =  usageAmount
                    )
                    usageRecordList.add(usageRecord)
                }
            }
        }

        DBOperator.saveUsageRecords(usageRecordList)

        val result = JSONObject()
        result["data"] = "upload successful"
        val jsonResult = result.toJSONString()
//        logger.info(jsonResult)
        channelHandlerContext.writeAndFlush(buildSuccessResponse(jsonResult))
        channelHandlerContext.close()
        logger.info("Request: $jsonBody  Result: $jsonResult    spend ${System.currentTimeMillis() - startTimeMills}ms")
    }
}
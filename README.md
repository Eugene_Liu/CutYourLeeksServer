# ob-cost-metrics-collector

version: 0.1

### Function
1. Get data from http interface and kafka.
2. Save data to time series database.

### Dependencies


### DB Options
1. Tidb
2. VictoriaMetrics

### VM options:
```
-Dkotlinx.coroutines.debug
-XX:+DoEscapeAnalysis
-XX:+HeapDumpOnOutOfMemoryError
-XX:HeapDumpPath=logs/heapdump.hprof
-Dio.netty.tryReflectionSetAccessible=true
--add-opens
java.base/jdk.internal.misc=ALL-UNNAMED
--add-opens
java.base/java.nio=ALL-UNNAMED
```